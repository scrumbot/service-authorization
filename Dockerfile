FROM maven:3.5.3-jdk-8-alpine as target
WORKDIR /build
COPY pom.xml .

COPY src/ /build/src/
RUN mvn package

FROM openjdk:8-jre-alpine
EXPOSE 8000
COPY --from=target /build/target/service-authorization.jar /app/service-authorization.jar
CMD exec java -jar -Djava.security.egd=file:/dev/./urandom /app/service-authorization.jar